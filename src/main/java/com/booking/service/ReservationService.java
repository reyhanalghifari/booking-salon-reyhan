package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
	
    public static void createReservation(List<Person>personList, List<Reservation> reservationList, int reservationId, String customerId, String employeeId, List<Service> services,
            String workstage){
    	Customer customer = getCustomerByCustomerId(personList, customerId);
    	Employee employee = getEmployeeByEmployeeId(personList, employeeId);
    	
    	Reservation reservation = new Reservation(ValidationService.generateResIdString(reservationId), customer, employee, services, 0, workstage);
    	reservationList.add(reservation);
    }

//    public static Customer getCustomerByCustomerId(List<Person>personList, String id){
//        for(int i=0; i<personList.size(); i++) {
//        	Customer customer = (Customer) personList.get(i);
//        	if(id.equalsIgnoreCase(customer.getId())) {
//        		return customer;
//        	}
//        }
//		return null;
//    }
    
    public static Customer getCustomerByCustomerId(List<Person>personList, String id){
        int result = 0;
    	for(int i=0; i<personList.size(); i++) {
    		Customer customer = (Customer) personList.get(i);
        	if(id.equalsIgnoreCase(customer.getId())) {
        		result = i;
        		break;
        	}
        }
		return (Customer) personList.get(result);
    }
   
    public static Employee getEmployeeByEmployeeId(List<Person>personList, String id){
        for(Person person : personList) {
        	if(person instanceof Employee) {
        	Employee employee = (Employee) person;
        	if(id.equalsIgnoreCase(employee.getId())) {
        		return employee;
        		}
        	}
        }
		return null;
    }
    
    public static List<Service> getServiceByServiceId(List<Service> serviceList, String serviceId){
    	for(Service service : serviceList) {
        	if(!serviceId.equalsIgnoreCase(service.getServiceId())) {
        		return serviceList;
        	}
        }
		return serviceList;
    }
    
    public static void editReservationWorkstage(){
        
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
