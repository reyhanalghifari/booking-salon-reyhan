package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static List<Reservation> reservationList = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/cancel reservation", "Exit"};
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "Show History Reservation + Total Profit", "Back to main menu"};
    
        int optionMainMenu;
        int optionSubMenu;
        int reservationId = 1;
        String messagePilihan = "Ingin Pilih Service yang Lain? (Y/T)";
        boolean isLooping = false;
        
		boolean backToMainMenu = false;
        boolean backToSubMenu = false;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = Integer.valueOf(input.nextLine());
            switch (optionMainMenu) {
                case 1:
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = Integer.valueOf(input.nextLine());
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                // panggil fitur tampilkan recent reservation
                            	PrintService.showRecentReservation(reservationList);
                                break;
                            case 2:
                                PrintService.showAllCustomer(personList);
                                break;
                            case 3:
                                PrintService.showAllEmployee(personList);
                                break;
                            case 4:
                                // panggil fitur tampilkan history reservation + total keuntungan
                                break;
                            case 0:
                                backToSubMenu = true;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                    // panggil fitur menambahkan reservation
                	System.out.print("Masukan customer ID: ");
                	String customerId;
                	customerId = input.nextLine();
                	Customer customer = ReservationService.getCustomerByCustomerId(personList, customerId);
                	System.out.print("Masukan Employee ID: ");
                	String employeeId;
                	employeeId = input.nextLine();
                	Employee employee = ReservationService.getEmployeeByEmployeeId(personList, employeeId);
                	System.out.print("Masukan service ID: ");
                	String serviceId;
                	serviceId = input.nextLine();
                	List<Service> services = ReservationService.getServiceByServiceId(serviceList, serviceId);
                	isLooping = konfirmasiPilihanMenu(messagePilihan);
                	String workstage = "";
                	for (Reservation reservation : reservationList) {
                	workstage = reservation.getWorkstage();
                    }
                	ReservationService.createReservation(personList, reservationList, reservationId, customerId, employeeId, services, workstage);
                    reservationId++;
                	break;
                case 3:
                    // panggil fitur mengubah workstage menjadi finish/cancel
                    break;
                case 0:
                    backToMainMenu = true;
                    break;
            }
        } while (!backToMainMenu);
	}
    public static boolean konfirmasiPilihanMenu(String question) {
		boolean isLooping = true;
		Scanner input = new Scanner(System.in);
		System.out.println(question);
		String pilihan = input.nextLine();
		if (pilihan.equalsIgnoreCase("T")) {
			isLooping = false;
		}
		
		return isLooping;
	}
}
